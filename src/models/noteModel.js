const mongoose = require("mongoose");

const NoteSchema = mongoose.Schema({
    userId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    completed: {
      type: Boolean,
      default: false,
    },
    text: {
      type: String,
      required: true,
    },
    createdDate: {
      type: String,
      default: new Date(),
    },
});

module.exports = mongoose.model("Note", NoteSchema);
