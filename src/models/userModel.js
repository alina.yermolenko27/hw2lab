const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: new Date(),
  }
});

module.exports = mongoose.model("User", UserSchema);
