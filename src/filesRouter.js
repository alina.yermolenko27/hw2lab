const express = require('express');
const router = express.Router();

const { getUser, deleteUser, changeUserPassword } = require("./controllers/user");
const { getNotes, createNote, getNoteById, updateNoteById, checkNoteById, deleteNoteById } = require("./controllers/notes");
const { registerUser, loginUser } = require("./controllers/auth");


router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);

router.get('/users/me', getUser);
router.delete('/users/me', deleteUser);
router.patch('/users/me', changeUserPassword);


router.get('/notes', getNotes);
router.post('/notes', createNote);
router.get('/notes/:id', getNoteById);
router.put('/notes/:id', updateNoteById);
router.patch('/notes/:id', checkNoteById);
router.delete('/notes/:id', deleteNoteById);

module.exports = {
  filesRouter: router
};
