const User = require("../models/userModel");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');

const registerUser = async (req, res) => {
  try {
    if (!req.body.username || !req.body.password) {
      return res.status(400).send({ "message": "Invalid name or password" });
    }

    let userData = new User({
      username: req.body.username,
      password: await bcrypt.hash(req.body.password, 10),
      createdDate: new Date(),
    });

    await userData.save()
    return res.status(200).send({ "message": `Success` })
  }

  catch (err) {
    console.log(err)
    return res.status(500).send({ "message": "Server error" })
  }

}

const loginUser = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username })
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
      const payload = {
        _id: user._id,
        createdDate: user.createdDate,
      }
      
      let jwtSecretKey = process.env.JWT_SECRET_KEY;
      const jwtToken = jwt.sign(payload, jwtSecretKey)
      return res.status(200).json({ 'message': 'Success', "jwt_token": jwtToken });
    }
    else {
      return res.status(400).json({ 'message': 'not authorized' });
    }
  }
  catch (err) {

    console.error(err)
    return res.status(500).json({ 'message': 'Server error' });
  }
}

module.exports = {
  loginUser,
  registerUser,
};