let token;
const form = document.getElementById('form');
const register = document.getElementById('register');
const login = document.getElementById('login');
const responseDiv = document.getElementById('response');
const acount = document.getElementById('acount');
const getAcount = document.getElementById('getAcount');
const deleteAcount = document.getElementById('deleteAcount');
const changePassword = document.getElementById('changePassword');
const userInfo = document.getElementById('userInfo');
const notes = document.getElementById('notes');
const notesList = document.getElementById('notesList');
const addNote = document.getElementById('addNote');
const note = document.getElementById('note');

const getNote = document.getElementById('getNote');
const changeNote = document.getElementById('changeNote');
const noteCheck = document.getElementById('noteCheck');
const deleteNote = document.getElementById('deleteNote');

register.addEventListener('click', (e) => {
  let username = document.getElementById('username').value
  let password = document.getElementById('password').value
  if (username.length >= 5 && password.length >= 5) {
    registerAccount(username, password)
  }

  responseDiv.innerHTML = 'Username or password is too short'

})

login.addEventListener('click', (e) => {
  let username = document.getElementById('username').value
  let password = document.getElementById('password').value
  enterAccount(username, password)
})

getAcount.addEventListener('click', (e) => {
  getInfo()
  async function getInfo() {
    const response = await fetch('http://localhost:8080/api/users/me', {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
    })
    if (response.status === 200) {
      let info = await response.json();
      userInfo.innerHTML =
        `<h1>${info.user.username}</h1>
          <div> User id ${info.user._id}</div>
        <div>User createdate ${info.user.createdDate}</div>`
    }
  }
})

async function registerAccount(username, password) {
  const response = await fetch('http://localhost:8080/api/auth/register', {
    method: 'POST',
    credentials: 'same-origin',
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      "username": username,
      "password": password,
    })
  })
  if (response.status === 400) {
    responseDiv.innerHTML = 'Please provide username and password to register'
  }
  if (response.status === 500) {
    responseDiv.innerHTML = 'Invalid name'
  }
  if (response.status === 200) {
    responseDiv.innerHTML = 'Registered succesfully'
  }
}

async function enterAccount(username, password) {
  let response = await fetch('http://localhost:8080/api/auth/login', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      "username": username,
      "password": password,
    })
  })


  if (response.status === 400) {
    responseDiv.innerHTML = 'Username or password is wrong'
  }
  if (response.status === 200) {
    response = await response.json()
    token = response.jwt_token;
    responseDiv.innerHTML = 'Enter successfully';
    form.style.display = "none";
    acount.style.visibility = "visible";
  }
}

deleteAcount.addEventListener('click', (e) => {
  deleteAcc()
  async function deleteAcc() {
    const response = await fetch('http://localhost:8080/api/users/me', {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
    })

    if (response.status === 200) {
      location.reload(true)
    }
  }
})

changePassword.addEventListener('click', (e) => {
  const oldPassword = document.getElementById('oldPassword').value;
  const newPassword = document.getElementById('newPassword').value;

  async function changePass(oldPassword, newPassword) {
    const response = await fetch('http://localhost:8080/api/users/me', {
      method: 'PATCH',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        "oldPassword": oldPassword,
        "newPassword": newPassword,
      })
    })

    if (response.status === 200) {
      changedInfo.innerHTML = 'Password successfuly changed'
    }
    if (response.status === 400) {
      changedInfo.innerHTML = 'Wrong password'
    }
  }

  changePass(oldPassword, newPassword)
})

notes.addEventListener('click', (e) => {
  notesList.innerHTML = ""
  getNotes()
  async function getNotes() {
    const response = await fetch('http://localhost:8080/api/notes', {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
    })

    let info = await response.json();

    if (response.status === 200) {
      for (let note of info.notes) {
        if (!info.notes.includes(note._id)) {
          notesList.innerHTML += `
          <li>
          <h3>${note.text}</h3>
          <p>Note number: ${note._id}</p>
          <p>Note completed: ${note.completed}</p>
          </li>  
          `
        }
      }
    }
    if (response.status === 400) {
      notesList.innerHTML = 'No notes'
    }
  }
})

addNote.addEventListener('click', (e) => {
  const newNote = document.getElementById('newNote').value;
  addOneNote()
  async function addOneNote() {
    const response = await fetch('http://localhost:8080/api/notes', {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        "text": newNote,
      })
    })

    if (newNote.length <= 0) {
      note.innerHTML = `Note is epmty`
      return
    }

    if (response.status === 200) {
      note.innerHTML = `Note ${newNote} added succesfully`;
    }
    if (response.status === 400) {
      note.innerHTML = 'No notes';
    }
  }

})

getNote.addEventListener('click', (e) => {
  const noteNumber = document.getElementById('noteNumber').value;
  getOneNote()

  async function getOneNote() {
    const response = await fetch(`http://localhost:8080/api/notes/${noteNumber}`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
    })

    let info = await response.json();
    if (noteNumber === undefined) {
      note.innerHTML = 'Please provide note number';
      return;
    }
    if (response.status === 200) {
      note.innerHTML =
        `<h3>${info.note.text}</h3>
          <p>Note number: ${info.note._id}</p>`
    }
    if (response.status === 400) {
      note.innerHTML = 'No such note'
    }
  }
})

changeNote.addEventListener('click', (e) => {
  const noteNumber = document.getElementById('noteNumber').value;
  const newNote = document.getElementById('newNote').value;

  changeOneNote()
  async function changeOneNote() {
    const response = await fetch(`http://localhost:8080/api/notes/${noteNumber}`, {
      method: 'PUT',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        "text": newNote,
      })
    })

    if (response.status === 200) {
      note.innerHTML =
        `<h3>Note ${noteNumber} changed to ${newNote}</h3>`
    }
    if (response.status === 400) {
      note.innerHTML = 'No such note'
    }
    if (response.status === 404) {
      note.innerHTML = 'Please provide note number'
    }
  }
})

noteCheck.addEventListener('click', () => {
  const noteNumber = document.getElementById('noteNumber').value;

  checkOneNote()
  async function checkOneNote() {
    const response = await fetch(`http://localhost:8080/api/notes/${noteNumber}`, {
      method: 'PATCH',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
    })

    let info = await response.json();
  
    if (response.status === 200) {
      note.innerHTML =
        `<p>Note Checked</p>`
    }
    if (response.status === 400) {
      note.innerHTML = 'No such note'
    }
  }
})

deleteNote.addEventListener('click', (e) => {
  const noteNumber = document.getElementById('noteNumber').value;
  deleteOneNote()
  async function deleteOneNote() {
    const response = await fetch(`http://localhost:8080/api/notes/${noteNumber}`, {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json"
      },
    })

    if (response.status === 200) {
      note.innerHTML = 'Note deleted succesfully'
    }
    if (response.status === 400) {
      note.innerHTML = 'Note not deleted'
    }
  }
})